import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class FizzBuzz_Checker_Test {
    @Test
    void falseIfNotFizz(){
        Assertions.assertEquals("7",FizzBuzz_Checker.checkFizz(7));
    }
    @Test
    void trueIfFizz(){
        Assertions.assertEquals("FIZZ",FizzBuzz_Checker.checkFizz(12));
    }
    @Test
    void checkNumberIsGreaterThanZero(){
        Boolean isFizzBuzz = FizzBuzz_Checker.checkValidNumber(-4);
        Assertions.assertFalse(isFizzBuzz);
    }
    @Test
    void ifNotBuzz(){
        Assertions.assertEquals("8",FizzBuzz_Checker.checkBuzz(8));
    }
    @Test
    void ifBuzz(){
        Assertions.assertEquals("BUZZ",FizzBuzz_Checker.checkBuzz(10));
    }
}
