public class FizzBuzz_Checker {
    private static int num;

    public FizzBuzz_Checker(int number){

        this.num = number;
    }


    public static String checkFizz(int number) {
        if (number % 3 != 0) return Integer.toString(number);
        return "FIZZ";
    }

    public static Boolean checkValidNumber(int number) {
        if (number < 0) return false;
        return null;
    }

    public static String checkBuzz(int number) {
        if (number % 5 == 0) return "BUZZ";
        return Integer.toString(number);
    }
}
